using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Board : MonoBehaviour
{
    float[] pos = { -2f, -1.35f, -0.7f, -0.05f, 0.6f, 1.25f, 1.9f, 0f };    
    List<Tile> tileLst = new();
    List<Tile> tiles = new();
    List<GameObject> gObjLst = new();
    public GameObject holder;
    public GameObject board;
    public GameObject destroyed;
    public bool isLose = false;
    public bool isWin = false;
    public GameObject winMenu;
    public GameObject loseMenu;
    private SpriteRenderer spriteR;
   


    void Start()
    {
        Time.timeScale = 1f;
        tileLst = this.GetComponent<LoadMap>().tileLst;
        Debug.Log("tileLst: " + tileLst.Count);

        //lay list tat ca object
        foreach (Transform child in transform)
        {
            gObjLst.Add(child.gameObject);
        }

        foreach(var g in gObjLst)
        {
            spriteR = g.GetComponent<SpriteRenderer>();
            Tile tileTemp = tileLst.Find(s => s.Name == g.name);
            tileTemp.TileID = spriteR.sprite.name;
        }
    }


    // Update is called once per frame
    void Update()
    {        
        //check lose
        if (tiles.Count > 7) isLose = true;

        // check win
        List<GameObject> chkWinHolderLst = new();
        List<GameObject> chkWinObjLst = new();
        foreach (Transform child in holder.transform)
        {
            chkWinHolderLst.Add(child.gameObject);
        }
        foreach (Transform child in transform)
        {
            chkWinObjLst.Add(child.gameObject);
        }
        if (chkWinHolderLst.Count==0 && chkWinObjLst.Count==0) isWin = true;

        //Sort
        tiles = tiles.OrderBy(t => t.TileID).ToList();

        //reset
        int j = 0;
        foreach (var t in tiles)
        {

            gObjLst.FirstOrDefault(g => g.name == t.Name).transform.parent = holder.transform;
            gObjLst.FirstOrDefault(g => g.name == t.Name).transform.position = new Vector2(1000, 1000);
            gObjLst.FirstOrDefault(g => g.name == t.Name).GetComponent<SpriteRenderer>().sortingOrder = j + 1;
            j++;
        }

        //Print
        int i = 0;
        foreach (var t in tiles)
        {

            gObjLst.FirstOrDefault(g => g.name == t.Name).transform.parent = holder.transform;
            gObjLst.FirstOrDefault(g => g.name == t.Name).transform.position = new Vector2(pos[i], -2.7f);
            gObjLst.FirstOrDefault(g => g.name == t.Name).GetComponent<SpriteRenderer>().sortingOrder = i + 1;
            i++;
        }

        if (Input.GetMouseButtonUp(0))
        {
            //match 3
            int n = 1;
            int k = 1;
            for (int t=0; t< tiles.Count; t++)
            {
                if (tiles[k].TileID == tiles[k - 1].TileID)
                {
                    n++;
                    if (n == 3)
                    {

                        // destroy
                        gObjLst.FirstOrDefault(g => g.name == tiles[k].Name).transform.parent = destroyed.transform;
                        gObjLst.FirstOrDefault(g => g.name == tiles[k].Name).transform.position = destroyed.transform.position;

                        gObjLst.FirstOrDefault(g => g.name == tiles[k - 1].Name).transform.parent = destroyed.transform;
                        gObjLst.FirstOrDefault(g => g.name == tiles[k - 1].Name).transform.position = destroyed.transform.position;

                        gObjLst.FirstOrDefault(g => g.name == tiles[k - 2].Name).transform.parent = destroyed.transform;
                        gObjLst.FirstOrDefault(g => g.name == tiles[k - 2].Name).transform.position = destroyed.transform.position;

                        tiles.RemoveAt(k);
                        tiles.RemoveAt(k - 1);
                        tiles.RemoveAt(k - 2);
                    }
                }
                else
                {
                    n = 1;
                }
                k++;
            }
            
        }


        if (Input.GetMouseButtonDown (0))
        {
            RaycastHit2D[] hit = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.Count()!= 0)
            {
                List<Tile> hits = new();
                foreach(var h in hit)
                {
                    hits.Add(tileLst.Find(s => s.Name == h.collider.name));
                }

                hits = hits
                    .OrderByDescending(t => t.Layer)
                    .ToList();

                Tile tile = hits.First();
                if (tile.CurState == TileState.NORMAL) tiles.Add(tile);
                else Debug.LogWarning("This tile has been blocked");           
                

            }

        }

        if (isLose == true) Losed();
        if (isWin == true) Finished();
    }

    public void Losed()
    {
        loseMenu.SetActive(true);
        Time.timeScale = 0;
    }

    public void Finished()
    {
        winMenu.SetActive(true);
        Time.timeScale = 0;
    }
}
