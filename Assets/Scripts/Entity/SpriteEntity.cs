﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteEntity
{
    public int ID;
    public int Theme;
    public int Color;
    public string Image;
    public float ItemID;    
}