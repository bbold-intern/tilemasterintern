using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile
{
    public string Name { get; set; }
    public int Map { get; set; }
    public int TileCount { get; set; }
    public string TileID { get; set; }
    public int Layer { get; set; }
    public Transform Pos { get; set; }
    public int Color { get; set; }
    public TileState CurState { get; set; }

    
    public Tile(int map, int layer, Transform pos, int tileID, int color)
    {
        this.Map = map;
        this.Layer = layer;
        this.Pos = pos;
        this.TileCount = TileCount;
        this.Color = color;
    }

    public Tile()
    {
       
    }

    public Tile(int layerCount, int tileCount, Transform pos, string name, TileState curState)
    {
        this.Layer = layerCount;
        this.TileCount = tileCount;
        this.Pos = pos;
        this.Name = name;
        this.CurState = curState;
    }
}
