using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public Animator transition;
    public float transitionTime = 1f;
    public GameObject pauseMenu;

    // Update is called once per frame
    void Start()
    {           
    }

    public void LoadMainLevel()
    {
        _ = StartCoroutine(LoadLevel(1));
    }

    public void LoadMenuLevel()
    {
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
        _ = StartCoroutine(LoadLevel(0));
    }

    IEnumerator LoadLevel(int levelIndex)
    {
        transition.SetTrigger("start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(levelIndex);
    }
}
