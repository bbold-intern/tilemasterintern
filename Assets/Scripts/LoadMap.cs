using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadMap : MonoBehaviour
{
    public GameObject board;
    public List<Tile> tileLst;
    public int levelId;

    public LoadMap()
    {

    }
    void Awake()
    {
        if (PlayerPrefs.GetInt("levelId") <= 0)
        {
            PlayerPrefs.SetInt("levelId", 1);
        }
        levelId = PlayerPrefs.GetInt("levelId");
        Debug.LogWarning("levelId: " + levelId);
        MapBuilder mapBuilder = new MapBuilder();
        tileLst = mapBuilder.BuildMap(levelId.ToString(), board);       
    }

    void Update()
    {
        
    }
}
 