using LitJson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MapBuilder 
{
    private TextAsset txtMap;
    public MapJON mapJon;

    public List<Tile> BuildMap(string mapId, GameObject board)
    {
        //call function
        _ = loadMapData(mapId);
        int allTile = countAllTile(mapId);
        Debug.Log("All tile: "+allTile);
        SpriteRandomnize spriteRng = new SpriteRandomnize(allTile);

        //list sprite
        List<SpriteEntity> choosenSprite = spriteRng.getRandomSprite(allTile);
        List<int> breakdownLst = spriteRng.TileMatchBreakdown(allTile);

        //GetAllTileImage
        List<Sprite> listSprite = GetAllTileImage(breakdownLst, choosenSprite);
        listSprite.Shuffle();
        List<Tile> tileLst = MapPrint(board, listSprite);

        return tileLst;
    }

    public MapJON loadMapData(string mapId)
    {
        txtMap = Resources.Load<TextAsset>("Map/mapconfigs/" + mapId);        
        mapJon = new MapJON();
        mapJon = JsonMapper.ToObject<MapJON>(json: this.txtMap.ToString());
        mapJon = sortTileList(mapJon);
        return mapJon;
    }

    public MapJON sortTileList(MapJON mapJon)
    {
        for(int i=0;i< mapJon.l.Count; i++)
        {
            List<List<int>> lst = mapJon.l[i].t;
            lst = lst
                .OrderBy(arr => arr[0])
                .ThenByDescending(arr => arr[1])
                .ToList();
            mapJon.l[i].t = lst;
        }
        
        return mapJon;
    }

    public int countAllTile(string mapId)
    {
        int allTile = 0;
        mapJon = loadMapData(mapId);
        foreach (var l in mapJon.l)
        {
            foreach (var t in l.t)
            {
                allTile++;
            }
        }
        return allTile;
    }

    public List<string> GetBreakdownList(List<int> breakdownLst, List<SpriteEntity> choosenSprite)
    {        
        List<string> allListSprites = new();
        int increment = 0;
        int j = 0;
        foreach (var b in breakdownLst)
        {
            for (int i = 0; i < breakdownLst[increment]; i++)
            {                
                if (increment == 0)
                {                    
                    for(int x = 0; x < 3; x++)
                    {                        
                        allListSprites.Add(choosenSprite[0].Image);
                    }                                       
                }
                else if (increment == 1)
                {                    
                    allListSprites.Add(choosenSprite[0].Image);
                    allListSprites.Add(choosenSprite[0].Image);
                }
                else
                {                    
                    allListSprites.Add(choosenSprite[0].Image);                    
                }
                choosenSprite.RemoveAt(0);                
                j++;
            }
            increment++;
        }        
        //Debug.Log("allListSprites: " + allListSprites.Count);
        return allListSprites;
    }

    //convert those Sprite entity to list sprite
    public List<Sprite> GetAllTileImage(List<int> breakdownLst, List<SpriteEntity> choosenSprite)
    {
        List<string> getBreakdownList = GetBreakdownList(breakdownLst, choosenSprite);
        Sprite[] sprite = Resources.LoadAll<Sprite>("Sprite/SpriteAtlasTexture-tiles_02-2048x2048-fmt47");
        List<Sprite> allSprite = new List<Sprite>();

        foreach(var g in getBreakdownList)
        {
            foreach (var s in sprite)
            {
                if (s.name == g)
                {
                    allSprite.Add(s);
                    allSprite.Add(s);
                    allSprite.Add(s);
                }
            }
        }
        //shuffle
        allSprite.Shuffle();
        //Debug.Log("allSprite: " + allSprite.Count);
        return allSprite;
    }

    public void addTileCollider(GameObject thisMap)
    {
        _ = thisMap.AddComponent<BoxCollider2D>();
        thisMap.GetComponent<BoxCollider2D>().offset = new Vector2(-0.09f, 0.09f);
        thisMap.GetComponent<BoxCollider2D>().size = new Vector2(1.25f, 1.25f);
        BoxCollider2D boxCollider = thisMap.GetComponent("BoxCollider2D") as BoxCollider2D;
    }



    public Tile createTileObject(int layerCount, int tileCount, GameObject board, List<int> t, List<Sprite> listSprite, int a)
    {
        float myScale = 36;
        float myPos = 154;              

        GameObject thisMap = new UnityEngine.GameObject(name: "tile: " + layerCount + "_" + tileCount);
        thisMap.transform.parent = board.transform;       

        thisMap.transform.position = new Vector3(t[0] / myPos, t[1] / myPos +1, t[2] / myPos);
        thisMap.transform.localScale = new Vector3(myScale, myScale, myScale);

        //set entity
        Tile tile = new(layerCount, tileCount, thisMap.transform, ("tile: " + layerCount + "_" + tileCount).ToString(), TileState.NORMAL);

        _ = thisMap.AddComponent<TileCollider>();
        addTileCollider(thisMap);

        SpriteRenderer renderer = thisMap.AddComponent<SpriteRenderer>();
        thisMap.GetComponent<SpriteRenderer>().sprite = listSprite[a];

        return tile;
    }

    public List<Tile> MapPrint(GameObject board, List<Sprite> listSprite)
    {        
        int a = 0;
        int layerCount = 0;
        int tileCount = 0;
        List<Tile> lstTile = new();

        foreach (var l in mapJon.l)
        {
            layerCount++;
            foreach (var t in l.t)
            {
                tileCount++;
                lstTile.Add(createTileObject(layerCount, tileCount, board, t, listSprite, a));
                a++;
            }
            tileCount = 0;
        }

        return lstTile;
    }
}

