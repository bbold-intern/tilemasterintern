using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class SpriteRandomnize
{
    private List<SpriteEntity> Sprite;
    private TextAsset txtTileMerge;

    public SpriteRandomnize(int allTile)
    {
        loadTileData();
        List<SpriteEntity> tileListed = limitTileList(allTile/3);
        tileListed.Shuffle();
    }

    public TextAsset loadTileData()
    {
        Sprite = new List<SpriteEntity>();
        txtTileMerge = Resources.Load<TextAsset>("Map/tileconfigs/Tiles");
        return txtTileMerge;
    }

    public List<SpriteEntity> tileMerge(TextAsset txtTileMerge)
    {
        List<SpriteEntity> tileDataObj = JsonMapper.ToObject<List<SpriteEntity>>(json: txtTileMerge.ToString());

        return tileDataObj;
    }

    public List<SpriteEntity> limitTileList(int limit)
    {
        List<SpriteEntity> tileDataObj = tileMerge(txtTileMerge);
        List<SpriteEntity> tileList = new List<SpriteEntity>();
        for (int i = 0; i < limit; i++)
        {
            tileList.Add(tileDataObj[i]);
        }

        return tileList;
    }

    //breakdown
    public List<int> TileMatchBreakdown(int allTile)
    {
        int tripleSprite = 0;
        int doubleSprite = 0;
        int singleSprite = 0;
        int everySprite = allTile / 3;
        List<int> a = new List<int>();
        //divided into

        for (int i = 0; i < 1000; i++)
        {
            if (everySprite - 3 >= 0)
            {
                everySprite = everySprite - 3;
                tripleSprite++;
            }
            else break;
        }
        a.Add(tripleSprite);

        for (int i = 0; i < 1000; i++)
        {
            if (everySprite - 2 >= 0)
            {
                everySprite = everySprite - 2;
                doubleSprite++;
            }
            else break;
        }
        a.Add(doubleSprite);

        if (everySprite == 1) singleSprite = 1;
        else singleSprite = 0;
        a.Add(singleSprite);

        return a;
    }

    //get random different sprite
    public List<SpriteEntity> getRandomSprite(int allTile)
    {        
        List<int> breakdownLst = TileMatchBreakdown(allTile);
        int rqSprite = breakdownLst[0]+ breakdownLst[1]+ breakdownLst[2];
        Debug.Log("rqSprite: " + breakdownLst[0] + "+"+ breakdownLst[1] + "+" + breakdownLst[2]);
        List<SpriteEntity> choosenSprite = new List<SpriteEntity>();
        List<SpriteEntity> lstSprite = limitTileList(23);
        lstSprite.Shuffle();

        //add different required sprite info from db to list
        for (int i=0; i<rqSprite; i++)
        {
            //check if color, theme....
            Debug.Log("lstSprite[i]: " + lstSprite[i].Image);
            choosenSprite.Add(lstSprite[i]);
        }
        Debug.Log("choosenSprite: " + choosenSprite.Count);

        return choosenSprite;
    }
}
