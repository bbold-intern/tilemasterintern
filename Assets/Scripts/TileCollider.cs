using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class TileCollider : MonoBehaviour
{
    Transform _transform;
    GameObject _gameObject;
    private List<Tile> tileLst;
    List<GameObject> GameObjList;
    private Tile thisTile;
    public bool isShadowed = false;
    public bool isDestroy = false;
    public bool isMoved = false;
    GameObject holder = new();

    void Start()
    {
        _transform = transform;
        _gameObject = _transform.gameObject;
        tileLst = GameObject.Find("Board").GetComponent<LoadMap>().tileLst;
        thisTile = tileLst.First(s => s.Name == _gameObject.name);
        holder = GameObject.Find("Board").GetComponent<Board>().holder;
        foreach (Transform child in holder.transform)
        {
            if (child.gameObject.name == thisTile.Name) isMoved = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        _transform = transform;
        _gameObject = _transform.gameObject;
        GameObjList = findOverlapCollider(_gameObject);
        getTopObject();
        setState();
    }

    private void setState()
    {
        if (isShadowed == true) thisTile.CurState = TileState.SHADOWED;
        if (isShadowed == false) thisTile.CurState = TileState.NORMAL;
        if (isDestroy == true) thisTile.CurState = TileState.DELETE;        
        if (isMoved == true) thisTile.CurState = TileState.MOVING;

        if (thisTile != null)
        {       

            switch (thisTile.CurState)
            {
                case TileState.NORMAL:
                    Color white = Color.white;
                    _gameObject.GetComponent<SpriteRenderer>().color = new Color(white.r, white.g, white.b, white.a);
                    break;

                case TileState.SHADOWED:
                    Color grey = Color.grey;
                    _gameObject.GetComponent<SpriteRenderer>().color = new Color(grey.r, grey.g, grey.b, grey.a);
                    break;

                case TileState.NONE:

                    break;

                case TileState.DELETE:
                    Destroy(_gameObject);
                    break;

                case TileState.MOVING:
                    _gameObject.GetComponent<Collider2D>().enabled = false;
                    break;
                default:
                    // code block
                    break;
            }            
        }
        else
        {
            Debug.LogError("This tile don't exit!!!");
        }
        
    }

    public void getTopObject()
    {
        List<Tile> compareTile = new();
        foreach (var gl in GameObjList)
        {
            foreach(var tl in tileLst)
            {
                if(gl.name == tl.Name)
                {
                    //Debug.Log("tileLst: " + tl.Layer);
                    compareTile.Add(tl);
                }
            }
        }
        
        compareTile = compareTile
                .OrderByDescending(arr => arr.Layer)
                .ToList();

        if (compareTile.Count == 0)
        {
            isShadowed = false;
            //Debug.Log("top tile = null");
        }
        else
        {
            Tile topTile = compareTile[0];
            if (topTile.Layer <= thisTile.Layer)
            {
                isShadowed = false;
            }
            else
            {
                isShadowed = true;
            }
        }
        


    }

    public List<GameObject> findOverlapCollider(GameObject gameObject)
    {
        BoxCollider2D myCollider = gameObject.GetComponent<BoxCollider2D>();
        
        List<Collider2D> colliderList = new();
        ContactFilter2D contactFilter = new();
        int colliderCount = myCollider.OverlapCollider(contactFilter.NoFilter(), colliderList);
        //Debug.LogWarning("colliderList: " + colliderList[0].gameObject);
        List<GameObject> overlapGameObjList = new();
        foreach(var l in colliderList)
        {            
            overlapGameObjList.Add(l.gameObject);
        }


        return overlapGameObjList;
    }

}


